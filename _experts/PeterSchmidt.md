---
title: "Peter Schmidt"
excerpt: "Podcaster, Scientist, Engineer"
header:
  teaser: assets/images/Peter-Schmidt.jpeg
sidebar:
  - title: "Code For Thought Podcast"
    image: assets/images/Peter-Schmidt.jpeg
    image_alt: "Peter Schmidt"
    text: "[https://codeforthought.buzzsprout.com](https://codeforthought.buzzsprout.com)"
  - title: "Contact"
    text: "[code4thought@proton.me](mailto:code4thought@proton.me)"
  - title: "Languages"                       
    text: "German, English, French"
---

### Open Science expertise: 
<!--PLEASE SELECT THE ONES THAT APPLY TO YOU:-->
Open Access Publishing, Open Source, Reproducibility, Research Software Engineering, Research Culture

### Services:
Producing, hosting and developing podcasts for scientific/engineering public engagement projects, training courses and classes

### Research background:
Experimental particle physics, medical physics and imaging, research software engineering

### Podcast expertise:
Creator, host and producer of the [Code for Thought](https://codeforthought.buzzsprout.com) podcast, a show to promote and strengthen the growing community of software engineers in science and scientists worldwide. Since its launch in January 2021, the show has been steadily growing and offers regular episodes on a wide range of subjects.

Producer/Host of the "ByteSized RSE" mini-series as part of [Code for Thought](https://codeforthought.buzzsprout.com) and the corresponding online [ByteSized RSE](https://www.imperial.ac.uk/computational-methods/rse/events/byte-sized-rse/) classes. 
"ByteSized RSE" is an initiative of the [Universe-HPC](https://excalibur.ac.uk/projects/universe-hpc/) project in the UK. 

[Code for Thought](https://codeforthought.buzzsprout.com) is available on all major podcast directories (Apple, Google, Spotify, Stitcher etc) as well as [YouTube](https://www.youtube.com/@code4thought) and [Soundcloud](https://on.soundcloud.com/qxjWR).

### Podcast Services
Podcasts are an incredibly popular and successful format. And while the focus is usually on news and entertainment, podcasting can also be highly effective in community building, education, training and scientific outreach.
As a podcast host and producer I offer the following services: 
- Training/Education: developing and producing podcast episodes alongside your training courses, classes and educational projects
- Public Engagement and Outreach: developing and producing material for your science, research and engineering projects
- Conferences/workshops: reporting on and covering workshops and conferences 

City: London <br/> 
Country: United Kingdom <br/>
&#x2611; Happy to offer services virtually <br/>
&#x2611; Happy to travel to places I can reach by train <br/>
{: .notice}
