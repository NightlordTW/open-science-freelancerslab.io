---
title: "Jo Havemann, PhD"
excerpt: "With a background in Evolution and Developmental Biology, Dr. Jo Havemann is a trainer and consultant in [Open] Science Communication and [digital] Science Project Management. Her work experience covers NGOs, a science startup and international institutions including the UN Environment Programme. With a focus on digital tools for science and her label Access 2 Perspectives, she aims at strengthening global science communication in general – and with a regional focus on Africa – through Open Science."
header:
  teaser: assets/images/Jo-Havemann.png
sidebar:
  - title: "Homepage"
    image: assets/images/Jo-Havemann.png
    image_alt: "Jo"
    text: "[https://access2perspectives.org/team/jo-havemann/](https://access2perspectives.org/team/jo-havemann/)"
  - title: "Contact"
    text: "[info@access2perspectives.org](mailto:info@access2perspectives.org)"
  - title: "Languages"                       
    text: "English, German"
---
### Open Science expertise: 
<!--PLEASE SELECT THE ONES THAT APPLY TO YOU:-->
Open Science Communication, Open Access Publishing, Open Project and Data Management (#beFAIRandCARE), Knowledge Transfer, Reproducibility/ Replicability, Research(er) Assessment, Global Equity/ Diversity/ Inclusivity in Research

### Services: 
<!--PLEASE SELECT THE SERVICES THAT YOU OFFER:-->
Speaker, Training, Consultancy, Research, Mentoring, Community building, Scholarly Editorial Services and Copy Editing

### Research background and expertise: 
<!--PLEASE ENTER YOUR AREA OF RESEARCH (e.g. Biomedical Sciences)-->
EvoDove, Cell Biology, Evolutionary Biology, Molecular Biology, Open Science, Open Scholarly Infrastructure <br/>
ORCID: [0000-0002-6157-1494](https://orcid.org/0000-0002-6157-1494)

### Speaker expertise: 
<!--Please enter free text on the talks that you offer or delete if you do not offer talks-->
Keynote speeches, participated and moderated panel discussions, 

### Training expertise: 
<!--Please enter free text on the training that you offer or delete if you do not offer training-->
Held research related soft skills training and workshops to international researcher groups, early career and later career stages alike.
Training topics include Open Science principles and practices, research Integrity, Career Development, Scholarly reading, Writing and Publishing, Research Project Management, for details, see [access2perspectives.org/topics/](https://access2perspectives.org/topics/) 

### Consultancy expertise: 
<!--Please enter free text on the consultancy services that you offer or delete if you do not offer consultancy-->
As the coordinating director of the African Open Access portal AfricArXiv (https://africarxiv.org) I have participated in consultancies at various global fora and stakeholder consortia re open scholarly infrastructure, Open Science principles and practices, Open Access and Research Project Management. My consultancy experience in Open Science strategy development and implementation is targeted to individual researchers, research departments as well as research organisations. 

### Additional info: 
<!--Please enter free text on or delete if you do not want to add additional info-->
Book a call at [calendly.com/access2perspectives/free-exploratory-session](https://calendly.com/access2perspectives/free-exploratory-session) so that we can explore if my services are a good fit for your requirements. I am looking forward to hearing from you.


City: Berlin and Nairobi <br/> 
Country: Germany and Kenya <br/> 
&#x2611; Happy to offer services virtually <br/>
&#x2611; Happy to travel in Europe and Africa <br/>
{: .notice}



