---
title: "Verena Heise"
excerpt: "Open Science Transformer"
header:
  teaser: assets/images/Verena-Heise.jpeg
sidebar:
  - title: "Homepage"
    image: assets/images/Verena-Heise.jpeg
    image_alt: "Verena"
    text: "[https://vheise.com](https://vheise.com/)"
  - title: "Contact"
    text: "[verena@heise.com](mailto:verena@heise.com)"
  - title: "Languages"                       
    text: "English, German"
---
### Open Science expertise: 
Open Access Publishing, Open Data/ Methods, Reproducibility/ Replicability, Research Culture, Research(er) Assessment

### Services: 
Speaker, Training, Consultancy, Event Organisation, Event Moderation, Research

### Research background: 
Neuroscience, Epidemiology, Biomedical Sciences

### Speaking expertise: 
I have give many talks (lectures, seminars, webinars, workshops) on diverse Open Science topics. Just check my website for a collection. 

### Training expertise: 
I offer introductory courses for open and reproducible research in any research field and in-depth courses for integrating open and reproducible practices in biomedical sciences across the whole cycle of a research project.

### Consultancy expertise: 
I led the development of Road2Openness (https://road2openness.de/) and have consultancy expertise in Open Science strategy development and implementation for research performing organisations. Additional expertise includes grassroots Open Science community development in academic environments. I'm also happy to help group leaders implement open and reproducible research practices in their labs. 

### Event organisation expertise: 
I have organised many events, both educational events and conferences, from brief workshops to 5-day summer schools. I'm happy to develop concepts for Open Science events, have many connections to potential speakers and I can help with the moderation as well (see below).  

### Event moderation expertise: 
I have moderated a number of training events for Open Science, including all day conferences. 

### Research expertise: 
I currently work on reviews of reproducibility/ open science practices in biomedical sciences and on case studies of integrating Open Science in research performing organisations.

### Additional info: 
Unsure whether I’m the right person for your job? Just contact me so we can have an informal chat. 


City: Gladbeck <br/> 
Country: Germany <br/>
&#x2611; Happy to offer services virtually <br/>
&#x2611; Happy to travel worldwide <br/>
{: .notice}
