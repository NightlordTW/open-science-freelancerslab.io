---
title: "Thomas Debray"
excerpt: "Statistician ◆ Scientist ◆ Entrepreneur"
header:
  teaser: assets/images/Thomas-Debray.jpg
sidebar:
  - title: "Homepage"
    image: assets/images/Thomas-Debray.jpg
    image_alt: "Thomas Debray"
    text: "[https://www.fromdatatowisdom.com/](https://www.fromdatatowisdom.com/)"
  - title: "Contact"
    text: "[tdebray@fromdatatowisdom.com](mailto:tdebray@fromdatatowisdom.com)"
  - title: "Languages"                       
    text: "English, Dutch"
---


### Open Science expertise: 
Open Access Publishing, Open Data/ Methods, Open Source, Open Educational Resources, Reproducibility/ Replicability, Research Software Engineering, Research Culture, Research(er) Assessment

### Services: 
Consultancy, Training, Research, Statistical Software Development

### Research background: 
Biostatistics, Epidemiology, Machine Learning

### Training expertise: 
I offer workshops in the analysis of clincial trials and real-world data. For example, I delivered trainings in the following topics: network meta-analysis; meta-analysis of individual participant data; meta-analysis of prognostic studies; multiple imputation of missing data; developent and validation of risk prediction models; sample size estimation in clinical trials; principal stratum analysis; an introduction to Real-World Evidence.

### Consultancy expertise: 
I offer support in the conceptualization, design, analysis and interpretation of clincial trials and real-world evidence studies. I have particular expertise in the implementation of statistical methods for confounder adjustment, indirect treatment comparisons, (network) meta-analysis, risk prediction, joint modelling of longitudinal and survival outcomes.

### Research expertise: 
I develop statistical methods for analyzing real-world data and predicting individualized treatment effects.

City: Utrecht <br/> 
Country: The Netherlands <br/>
<!--Please delete/ change the following as appropriate-->
&#x2611; Happy to offer services virtually <br/>
&#x2611; Happy to travel worldwide <br/>
{: .notice}


