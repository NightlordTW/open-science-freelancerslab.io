---
title: "Chris Hartgerink"
excerpt: "Working to distribute power in research"
header:
  teaser: assets/images/Chris-Hartgerink.jpg
sidebar:
  - title: "Homepage"
    image: assets/images/Chris-Hartgerink.jpg
    image_alt: "Chris Hartgerink"
    text: "[https://libscie.org](https://libscie.org)"
  - title: "Contact"
    text: "[chris@libscie.org](mailto:chris@libscie.org)"
  - title: "Languages"                       
    text: "English, Dutch"
---
### Open Science expertise: 
<!--PLEASE SELECT THE ONES THAT APPLY TO YOU:-->
Open Access Publishing, Open Data/ Methods, Open Source, Citizen Science, Open Innovation, Open Educational Resources, Reproducibility/ Replicability, Research Software Engineering, Research Culture, Research(er) Assessment, Equity/ Diversity/ Inclusivity in Research

### Services: 
<!--PLEASE SELECT THE SERVICES THAT YOU OFFER:-->
Speaker, Consultancy, Research, panel moderation, website hosting + website building

### Research background: 
<!--PLEASE ENTER YOUR AREA OF RESEARCH (e.g. Biomedical Sciences)-->
Meta-research, social psycholoy, statistics, library- and information sciences

### Speaker expertise: 
<!--Please enter free text on the talks that you offer or delete if you do not offer talks-->
Research modules, research integrity, publishing lifecycle.

### Consultancy expertise: 
<!--Please enter free text on the consultancy services that you offer or delete if you do not offer consultancy-->
Detecting data fabrication

### Additional info: 
<!--Please enter free text on or delete if you do not want to add additional info-->
I offer a free 15 minute consult, which you can scheduled [through this link](https://savvycal.com/libscie-chartgerink/free-consult).

City: Berlin <br/> 
Country: Germany <br/>
<!--Please delete/ change the following as appropriate-->
&#x2611; Happy to offer services virtually <br/>
&#x2611; Happy to travel by train in Europe <br/>
{: .notice}



